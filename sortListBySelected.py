#!/usr/bin/python
import modo

# item in the variable named scene
scene = modo.Scene()

# Get selected items
selectedItems = scene.selected

# Using Python's built in sorted() command,
# alphabetize all of the root meshes by
# the .name property attached to the item
# the .lower() after the .name is used to
# force the name to lower case when the
# sort command reads the name.
# Without it, the sort would still work,
# but captial and lower case words would
# be separated.
sortedRootItems = sorted(selectedItems, key=lambda x: x.name.lower())

# Typical for loop, go through selectedItems
for item in selectedItems:
    # Test the item's .childCount() property
    # Only if the item has children, procees.
    # If not, we have nothing to sort.
    if item.childCount() != 0:
        # Get a list of all the children that are
        # also 'Mesh' objects
        childrenItems = item.children()
        # The rest is nearly identical to what we
        # performed above.
        sortedChildrenMeshes = sorted(childrenItems, key=lambda x: x.name.lower())

        for count, child in enumerate(sortedChildrenMeshes):
            # This line is the only exception, newParent
            # needs to be specified.  In this case, we
            # provide the items current parent.
            # If we left this empty, setParent()
            # would unparent the child from the parent.
            child.setParent(newParent=item, index=count)
